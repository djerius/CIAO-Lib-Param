# NAME

CIAO::Lib::Param - An interface to the CIAO parameter library.

# VERSION

version 0.10

# SYNOPSIS

    use CIAO::Lib::Param;

    my $pf = CIAO::Lib::Param->new( $filename, $mode );

    my $value = $pf->get( 'var1' );
    $pf->set( 'var1', $newvalue);

# DESCRIPTION

CIAO::Lib::Param is a Perl interface to the parameter library
(cxcparam) shipped with the Chandra Interactive Analysis of
Observations (CIAO) software package.  It implements an interface to
IRAF (Image Reduction and Analysis Facility) style parameter files.

This document does not fully discuss the format and usage of parameter
files; see [http://asc.harvard.edu/ciao/ahelp/parameter.html](http://asc.harvard.edu/ciao/ahelp/parameter.html) for
more information.

CIAO::Lib::Param provides both object oriented and quick and dirty
procedural interfaces.  See the ["OBJECT INTERFACE"](#object-interface) and ["PROCEDURAL
INTERFACE"](#procedural-interface) sections for the two interfaces.

The Perl interface presents unified error reports from the underlying
cxcparam library.  If an error is encountered, the module will throw
an exception via **croak()**.  For simple applications, simply not
catching the exception with `eval{}` will cause the application to
terminate.

For more complicated handling, one can use the exception (which is a
CIAO::Lib::Param::Error object) to determine more information about
what happened.

# INTERNALS

# PROCEDURAL INTERFACE

This interface is for quick and dirty parameter retrieval.  The
parameter file is opened for each call, so these should not be used
for repeated access to parameters.

- pget

        use CIAO::Lib::Param qw/ pget /;

        $pvalue  = pget( $filename, $pname );
        @pvalues = pget( $filename, @pnames );
        %params  = pget( $filename );

        $pvalue  = pget( $filename, $argv, $pname );
        @pvalues = pget( $filename, $argv, @pnames );
        %params  = pget( $filename, $argv );

    Read one or more parameter values.  The user is never queried for a
    parameter value.  Illegal values result in an exception being thrown.

    In the first form (called in scalar context), retrieve the value of a
    single parameter.

    In the second form (list context, parameter names), the values for the
    specified parameters are returned as a list.

    In the third form, retrieve all of the parameters from the file as a
    hash, keyed off of the parameter name.

    The `$filename` argument may optionally be followed by an arrayref, which
    should contain elements of the form `param=value`.  Typically this is
    used to allow command line argument assignment:

        %params = pget( $0, \@ARGV );

- pquery

        use CIAO::Lib::Param qw/ pquery /;

        [...]

    This is identical to =pget= except that the user is queried when
    necessary.

- pset

        use CIAO::Lib::Param qw/ pset /;

        pset( $filename, $pname1 => $pvalue1, $pname2 => $pvalue2, ... );

    Set the named parameters to the given values.

- pfind

        use CIAO::Lib::Param qw/ pfind /;
        $pfile = pfind( $name, $mode, $extn, $path )

    Find a parameter file. The `extn` and `path` arguments are lists of
    allowable extension and directories to attempt to search for parameter
    files.  For example, the default search used in the class constructor
    is

            extn = ".par .rdb $PFEXTN"
            path = "$PDIRS $PFILES $UPARM"

    Identifiers prefixed with `$` are recursively expanded in the run time
    environment.  For compatibility with IRAF $UPARM should be set to a
    single directory name with a trailing `/`.  `$PFILES` may be set to
    a space or `,` separated list of directories.

# OBJECT INTERFACE

## Constructor

- new

        $pf = CIAO::Lib::Param->new( $filename );
        $pf = CIAO::Lib::Param->new( $filename, $mode );
        $pf = CIAO::Lib::Param->new( $filename, $mode, @arglist );
        $pf = CIAO::Lib::Param->new( [ $filename, $argv0], $mode, @arglist );

    Create a new object and associate it with the specified parameter
    file.  See ["Finding Parameter Files"](#finding-parameter-files) for more information on how the
    path to the file is determined.

    **$mode** indicates the IRAF mode with which the file should be opened
    (it defaults to `rw` if not specified).  It should be one of the
    "IRAF-compatible" parameter-interface access modes (eg `r`, `rw`,
    `rH`, etc).  `@arglist` is a list of parameter settings that will
    override those given in the parameter file.  They are strings of the
    form of `par=value`.

    `$filename` is typically `$0`.  However, the underlying library uses
    _two_ arguments to determine the name of the parameter file.  In the
    (extremely) rare situation where you wish to use that functionality,
    pass the two names as elements of an anonymous array.  The underlying
    call has the following definition:

        paramopen( filename, argv, argc, filemode )

    The `$filename` parameter will be passed as `filename`.  The
    `$argv0` parameter, which is typically just `$0`, will be inserted
    into the `argv` array as the first element.

    **new** throws an exception via **croak** if there is an error.

    The parameter file is closed (and optionally updated) upon object
    destruction.

## Miscellaneous Parameter methods

- access

        $pf->access( $pname );

    This returns true if the named parameter exists.

- info

        ( $mode, $type, $value, $min, $max, $prompt ) =
                             $pf->info( $pname );

    Return various bits of information about the named parameter.  It
    throws an exception via **die** if the parameter is not found.

## Retrieving Parameter Values

The parameter library supports a large variety of data types.  Perl
really only has two: strings and double precision floating point
values, and it automatically converts between the two as needed.

To retrieve a parameter value, in most cases one simply calls **get**.

To ease re-coding older code into Perl, the other type specific
routines (`getX`) have been incorporated into the Perl interface,
even though they should rarely be used.  One effect of these routines is
that the value is first converted into the specified type (e.g. short)
before being passed to Perl.  This may be of some benefit in some
instances.

- get

        $pf->get( $pname );

    Retrieve the value of the named parameter.  In all cases but Boolean
    parameters this performs no conversion, returning the value as a
    string (remember that Perl will automatically convert this to a number
    as required).

    In the case of Boolean parameters (which are stored in the parameter
    file as the strings `yes` and `no`), they are converted to Perl's true
    and false values.

- getb

        $pf->getb( $pname );

    Retrieve the named parameter's value as a Boolean value.

- gets

        $pf->gets( $pname );

    Retrieve the named parameter's value as a short.

- geti

        $pf->geti( $pname );

    Retrieve the named parameter's value as an integer.

- getf

        $pf->getf( $pname );

    Retrieve the named parameter's value as a float.

- getd

        $pf->getd( $pname );

    Retrieve the named parameter's value as a double.

- getstr

        $pf->getstr( $pname );

    Retrieve the named parameter's value as a string.  This is identical
    to **get** except for the case of Boolean parameters.

## Setting Parameter Values

These routines update the parameter values in the **Param** object. The
parameter file is updated when the Param object is destroyed (if the
file was opened with the correct mode to permit writing).

In keeping with standard naming schemes for accessors, the function
prefixes have been renamed from `putX` to `setX`, although the others
are still available to ease porting code.

In general one should use **set**, which uses Perl's intrinsic
stringification mechanisms to convert the Perl value to a string which
is passed on to the parameter interface routines.  In all but the most
esoteric cases this should suffice.

- set \[put\]

        $pf->set( $pname, $value );

    Set the named parameter to the given value.  In the case that the
    parameter is boolean, and the value is not a string, it will
    automatically convert Perl booleans to those required by **cxcparam**.

- setb \[putb\]

        $pf->setb( $pname, $value );

    Set the named parameter to the given value.
    Convert the value to a Boolean.

- setd \[putd\]

        $pf->setd( $pname, $value );

    Set the named parameter to the given value.
    Convert the value to a double.

- seti \[puti\]

        $pf->seti( $pname, $value );

    Set the named parameter to the given value.
    Convert the value to an integer.

- sets \[puts\]

        $pf->sets( $pname, $value );

    Set the named parameter to the given value.
    Convert the value to a short.

- setstr \[putstr\]

        $pf->setd( $pname, $value );

    Set the named parameter to the given value.  Convert the value to a
    string.

## Miscellaneous methods

- getpath

        $path = $pf->getpath;

    This returns the path to the parameter file associated with the
    object.

- match

        $pmatch = $pf->match( $template );

    Return a list of the parameters which match the given template.  The
    list is returned as a CIAO::Lib::Param::Match object.  The template
    may be either the single character `*`, which returns all parameters,
    or a string where the following characters have special meanings:

    - ?

        match any one character

    - \*

        match anything, or nothing

    - \[&lt;c>...\]

        match an inclusive set

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-ciao-lib-param@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CIAO-Lib-Param](https://rt.cpan.org/Public/Dist/Display.html?Name=CIAO-Lib-Param)

## Source

Source is available at

    https://gitlab.com/djerius/ciao-lib-param

and may be cloned from

    https://gitlab.com/djerius/ciao-lib-param.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2005 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
